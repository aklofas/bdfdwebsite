axis         = require 'axis'
rupture      = require 'rupture'
autoprefixer = require 'autoprefixer-stylus'
js_pipeline  = require 'js-pipeline'
css_pipeline = require 'css-pipeline'
contentful   = require 'roots-contentful'
marked       = require 'marked'
moment       = require 'moment'


module.exports =
  ignores: ['readme.md', '*.sublime-*', '**/layout.*', '**/_*', '.gitignore', 'ship.*conf', 'views/page**.jade', 'views/footer.jade']

  extensions: [
    js_pipeline(files: [
      'assets/js/vendor/jquery.js',
      'assets/js/vendor/bootstrap.js',
      'assets/js/*.coffee'
    ], out: 'js/build.js', minify: true),
    css_pipeline(files: [
      'assets/css/vendor/bootstrap.css',
      'assets/css/vendor/weather-icons.css',
      'assets/css/vendor/weather-icons-wind.css',
      'assets/css/vendor/callout.css',
      'assets/css/*.styl'
    ], out: 'css/build.css', minify: true),
    contentful(
      access_token: '182f84c4b738190c6d9084b01b724a9d883ea36018860972eb23ff1769e08054'
      space_id: 'kqkzhz963gvn'
      content_types:
        team:
          id: 'team'
          sort: compareFunction = (a, b) -> a.name.split(' ')[1].localeCompare(b.name.split(' ')[1])
        announce:
          id: 'announce'
          sort: compareFunction = (a, b) -> a.name.localeCompare(b.name)
        news:
          id: 'news'
          filters: {'limit': '3', 'order': '-fields.date'}
        newsall:
          id: 'news'
          sort: compareFunction = (a, b) -> new Date(b.date).getTime() - new Date(a.date).getTime()
        content:
          id: 'content'
        media:
          id: 'media'
    )
  ]

  locals:
    markdown: marked
    moment: moment

  server:
    clean_urls: true

  stylus:
    use: [axis(), rupture(), autoprefixer()]
